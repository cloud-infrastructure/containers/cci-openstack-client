## cci-openstack-client

Container image created to facilitate access to the CERN Cloud Infrastructure:

A user guide for the cloud service can be found in [clouddocs](https://clouddocs.web.cern.ch/).

This container image is published to `registry.cern.ch` with different tags:

* Latest test version: `registry.cern.ch/cloud/cci-openstack-client:latest`
* Release by codename: `registry.cern.ch/cloud/cci-openstack-client:zed`
* Release by version: `registry.cern.ch/cloud/cci-openstack-client:zed-20240122`
* Release recommended by the CERN Cloud Infrastructure team: `registry.cern.ch/cloud/cci-openstack-client:cern`

## How-To

### Container engine

If you plan to run this container in mainstream operating systems such as Linux,
Microsoft Windows or Mac, you should be able to do it with different tools. The
main ones being `Docker` and `Podman`.

Both tools offer versions for the different platforms:

* `Docker`: [Linux](https://docs.docker.com/desktop/install/linux-install/),
  [Windows](https://docs.docker.com/desktop/install/windows-install/) and [Mac](https://docs.docker.com/desktop/install/mac-install/).
* `Podman`: Linux distributions usually make it available in [default repos](https://podman.io/docs/installation#installing-on-linux).
  Windows support is described in [Podman for Windows](https://github.com/containers/podman/blob/main/docs/tutorials/podman-for-windows.md)
  and Mac users can follow [macOS](https://podman.io/docs/installation#macos) instructions.

### Authentication

The container image is configured to be used with **kerberos by default**. This
requires to initialise a valid ticket before the commands are issue. In an
interactive session it can be done as follows:

```
podman run -it cci-openstack-client:zed /bin/bash
[root@50e9b531dbce /]# kinit youruser
[root@50e9b531dbce /]# export OS_PROJECT_NAME="Personal youruser"
[root@50e9b531dbce /]# openstack server list
```

Alternatively, users can rely on **username & password**. If kerberos
authentication method is not an option, users can override it by setting
an alternative authentication type such as `v3password`. The following
example shows how to show a list of servers with a different method:

```
podman run --env OS_PROJECT_NAME --env OS_PASSWORD --env OS_USERNAME \
--env OS_AUTH_TYPE=v3password cci-openstack-client:zed openstack image list
```

The previous command assumes the host environment has the variables
`OS_PROJECT_NAME`, `OS_PASSWORD` and `OS_USERNAME`.

Regardless of the authentication method used, `OS_PROJECT_NAME` is
a mandatory environment variable that needs to be set.

### Running commands

This sections covers some examples in different platforms. It assumes
the host machine has properly installed already a container engine as
suggested in the _Container engine_ section.

#### Create a Glance image from Linux with podman

Assuming you have the img in your host `Downloads` home folder, you could
upload it to Glance ussing the following single podman command:

```
podman run --env OS_PROJECT_NAME --env OS_PASSWORD --env OS_USERNAME \
--env OS_AUTH_TYPE=v3password -v ~/Downloads/:/mnt cci-openstack-image \
openstack image create blah --file /mnt/cirros-0.6.2-x86_64-disk.img
```

#### Create a Glance image from Windows with docker

Assuming you have installed [Docker for Windows](https://docs.docker.com/desktop/install/windows-install/)
based on WSL or Hyper-V, you can upload an image to Glance from the Windows terminal.

This example assumes the image to upload is available in the `Downloads` folder from
the user account:

```
PS C:\Users\user> docker run --env OS_PROJECT_NAME="Personal fernandl" --env OS_USERNAME=fernandl --env OS_AUTH_TYPE=v3password -it -v .\Downloads\:/mnt gitlab-registry.cern.ch/cloud-infrastructure/containers/cci-openstack-client:zed openstack image create image-from-windows --file /mnt/cirros-0.6.2-x86_64-disk.img
```

In this case, the command prompts the user to introduce the password. You can avoid that interactive step
by setting `OS_PASSWORD` as well.

Using `podman` is possible as well and the command will be the same, just replace `docker` by `podman` after
installing and initializing the podman machine as described in the upstream documentation.

#### Interactive shell with kerberos to run multiple commands from Windows

In case that is needed to open an interactive shell, the user can do as shown in the following snippet:

```
PS C:\Users\fernandl> docker run --env OS_PROJECT_NAME="Personal fernandl" -it -v .\Downloads\:/mnt gitlab-registry.cern.ch/cloud-infrastructure/containers/cci-openstack-client:zed /bin/bash
[root@6251b1bb95eb /]# kinit fernandl
Password for fernandl@CERN.CH:
[root@6251b1bb95eb /]# ls /mnt
cirros-0.6.2-x86_64-disk.img
[root@6251b1bb95eb /]# openstack image list --public
+--------------------------------------+-----------------------------------------------+--------+
| ID                                   | Name                                          | Status |
+--------------------------------------+-----------------------------------------------+--------+
| 5d71eed0-1c75-4e99-84d7-c9e0643e24b7 | ALMA8 - aarch64                               | active |
```

#### Interactive shell with kerberos from lxplus.cern.ch

As described in [KB0006874](https://cern.service-now.com/service-portal?id=kb_article&n=KB0006874),
you can pass your existing kerberos tickets from the lxplus host to the container.
Using that method, you can easily get an interactive session into the container
with credentials initialised:

```
(lxplus)$ podman run -v $XDG_RUNTIME_DIR/krb5cc:$XDG_RUNTIME_DIR/krb5cc:Z --env KRB5CCNAME=$XDG_RUNTIME_DIR/krb5cc --env OS_PROJECT_NAME -it registry.cern.ch/cloud/cci-openstack-client:mr_1 /bin/bash
[root@a1e59eada0d9 /]# openstack flavor list
+-------+-----------+------+------+-----------+-------+-----------+
| ID    | Name      |  RAM | Disk | Ephemeral | VCPUs | Is Public |
+-------+-----------+------+------+-----------+-------+-----------+
| 12076 | m2.large  | 7500 |   40 |         0 |     4 | True      |
| 17895 | m2.small  | 1875 |   10 |         0 |     1 | True      |
| 38242 | m2.medium | 3750 |   20 |         0 |     2 | True      |
+-------+-----------+------+------+-----------+-------+-----------+
```

## Troubleshooting

### OpenStack commands return "The service catalog is empty."

This error is very likely caused by the variable "OS_PROJECT_NAME"
being not set in the environment.

You can fix it by setting `OS_PROJECT_NAME` while invoking commands
through the container or while running interactively.


## Development

### Repository structure

The `main` branch has the latest version.

Branches with the pattern `stable/<codename>` track each OpenStack release and it is used
to backport changes if required.

Tagging is done against `stable/<codename>` branches.

### Image versioning

The container images will be versioned by the OpenStack release codename and the
date they were generated. Additionally, latest versions for each release will be
tagged with the codename.

Example:

| Version Tag       | Code Tag  |
| ----------------- |---------- |
| antelope-20240122 | antelope  |
| zed-20240122      | zed       |

There will be two additional tags. `cern` points to the version recommened to be
used against the CERN Cloud Infrastructure and `latest` pointing to the latest
version of the code in the `main` branch.
